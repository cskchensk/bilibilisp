package bilibili;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.dongliu.requests.Requests;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medivh on 2020/2/25.
 * 愿我出走半生,归来仍是少年
 */
@Setter
@Getter
@Slf4j
public class BilibiliUserInfo {
    String cookies = "";

    String uname = "";

    String follower = "";
    String face = "";
    String money = "";
    String mid = "";
    public BilibiliUserInfo(String cookies){
        this.cookies = cookies;
    }
    public void getUserInfo(){
        String url = "https://api.bilibili.com/x/web-interface/nav";
        HashMap response  = JSONObject.parseObject(this.get(url),HashMap.class);
        HashMap data  = JSONObject.parseObject(response.get("data").toString(),HashMap.class);
        this.uname = data.get("uname").toString();
        this.face =  data.get("face").toString();
        this.money =  data.get("money").toString();
        JSONObject object = (JSONObject) response.get("data");
        this.mid = object.getString("mid");
        url = "https://api.bilibili.com/x/relation/stat?jsonp=jsonp&vmid="+this.mid;
        response  =JSONObject.parseObject(this.get(url),HashMap.class);
        object= ((JSONObject)response.get("data"));
        this.follower =  object.get("follower").toString();

    }
    public void product(){

    }


    public String get(String url){

        Map<String,String> headers = new HashMap();
        headers.put("Accept","*/*");
        headers.put("Accept-Encoding","gzip, deflate, br");
        headers.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) Chrome/63.0.3239.132 Safari/537.36");
        headers.put("Cookie",this.cookies);
        String response =  Requests.get(url).headers(headers).send().readToText();
        return  response;

    }
    public  static  void  main(String args[]) throws IOException, InterruptedException {
        String bilibilicookies = "LIVE_BUVID=AUTO4715785517982541; DedeUserID=25437908;  SESSDATA=228af7e9%2C1585093835%2C48daa321; bili_jct=4d91f1d39d2a066feaf0c274622ad44c;";
        BilibiliUserInfo userInfo = new BilibiliUserInfo(bilibilicookies);
        userInfo.getUserInfo();


    }

}
